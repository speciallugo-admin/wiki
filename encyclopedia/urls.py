from unicodedata import name
from django.urls import path

from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("wiki/<str:title>",views.entry, name="entry"),
    path("newEntry", views.newEntry, name="newEntry"),
    path("wiki/<str:title>/editEntry", views.editEntry, name="editEntry"),
    path("randomEntry", views.randomEntry, name="randomEntry"),
    path("search", views.search, name="search")
]
