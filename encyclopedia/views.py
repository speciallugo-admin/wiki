from audioop import reverse
from random import choice
from django.shortcuts import render
from flask import redirect
from markdown import markdown
from . import util


# home page view
def index(request):
    
    return render(request, "encyclopedia/index.html", {
        "entries": util.list_entries()
    })

    
# entry page view
def entry(request, title):
    content = util.get_entry(title)
    if not content:
        return render(request, "encyclopedia/error.html", {
            "message": "\"" + title + "\" has not been added yet."
        })
    return render(request, "encyclopedia/entry.html", {
        "title": title,
        "content": markdown(content)
    })

# search entry page
def search(request):
    query = request.GET.get("q")
    content = util.get_entry(query)
    if not content:
        result = []
        for title in util.list_entries():
            if query.casefold() in title.casefold():
                result.append(title)
        return render(request, "encyclopedia/search.html", {
            "result": result
        })
    return render(request, "encyclopedia/entry.html", {
        "title": query,
        "content": markdown(content)
    })

# create a new entry form
def newEntry(request):
    if request.method == "POST":
        title = request.POST.get("title")
        content = request.POST.get("content")
        for entry in util.list_entries():
            if title.casefold() == entry.casefold():
                return render(request, "encyclopedia/create.html", {
                    "message": "This entry already exists!",
                    "title": title,
                    "content": content
                })
        util.save_entry(title, content)
        return render(request, "encyclopedia/index.html", {
            "entries": util.list_entries(),
        })
    return render(request, "encyclopedia/newEntry.html")

# edit entry page view
def editEntry(request, title):
    if request.method == "GET":
        content = util.get_entry(title)
        return render(request, "encyclopedia/editEntry.html", {"title": title, "content": content})

    if request.method == "POST":
        content = request.POST.get("edit_content")
        util.save_entry(title, content)
        return redirect("entry", title)

# using random choice to render random entry page
def randomEntry(request):
    title = choice(util.list_entries())
    content = util.get_entry(title)
    return render(request, "encyclopedia/entry.html", {
        "title": title,
        "content": markdown(content)
    })